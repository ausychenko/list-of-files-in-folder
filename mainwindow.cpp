#include <mainwindow.h>

void MainWindow::func(const QString &dir){

    static QString str = "";

    QDir qDir(dir);
    QStringList listDir = qDir.entryList();
    if (listDir.empty()) {
        str.remove(0,4);
        return;
    }
    qDir.setFilter(QDir::Dirs);
    listDir = qDir.entryList();
    listDir.removeFirst();
    listDir.removeFirst();

    for (auto i = 0; i < listDir.size(); i++){
        str.append("│   ");
        qPlainTextEdit->appendPlainText(str);
        qPlainTextEdit->insertPlainText("├── " + listDir[i]);
        func(qDir.absoluteFilePath(listDir[i]));
    }

    qDir.setFilter(QDir::Files);
    QFileInfoList fileNames = qDir.entryInfoList();
    for (auto & fileName : fileNames){
//        qDebug() << fileNames[i].fileName();
        qPlainTextEdit->appendPlainText(str);
        qPlainTextEdit->insertPlainText("└── " + fileName.fileName());
    }
    str.remove(0,4);
}

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent) {

    qPlainTextEdit = new QPlainTextEdit();
    qPlainTextEdit->setReadOnly(true);
    qPlainTextEdit->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);

    mainLayout = new QVBoxLayout;
    mainLayout->addWidget(qPlainTextEdit);

    auto *pQWidget = new QWidget();
    pQWidget->setLayout(mainLayout);

    setCentralWidget(pQWidget);
    resize(QSize(400, 800));
    setAcceptDrops(true);
}

MainWindow::~MainWindow()
= default;

void
MainWindow::dragEnterEvent(QDragEnterEvent *event) {
    Q_FUNC_INFO_ENTER
    if (event->mimeData()->hasUrls()) {
        event->acceptProposedAction();
    }
    Q_FUNC_INFO_EXIT
}

void
MainWindow::dropEvent(QDropEvent *event) {
    Q_FUNC_INFO_ENTER
    qPlainTextEdit->clear();
    QList<QUrl> urls = event->mimeData()->urls();
    for (auto & url : urls) {
        QString directory = url.toLocalFile();
        QDir qDir(directory);
        qDir.setFilter(QDir::Dirs);
        QStringList listDirs = qDir.entryList();
        qPlainTextEdit->insertPlainText("├── " + qDir.dirName());
        func(directory);
    }
    Q_FUNC_INFO_EXIT
}

void
MainWindow::dragMoveEvent(QDragMoveEvent *event) {
    event->accept();
}

void
MainWindow::dragLeaveEvent(QDragLeaveEvent *event) {
    event->accept();
}
