set qt_dir=C:\Qt\5.15.1\mingw81_64\bin
set projects_dir=C:\YandexDisk\Documents\NIITM\Projects\C++\list of files in folder
set exe_file_name=list_of_files_in_folder.exe

xcopy /f /y "%projects_dir%\cmake-build-release\%exe_file_name%" "%projects_dir%\standalone"
cd qt_dir
windeployqt.exe "%projects_dir%\standalone\%exe_file_name%"

xcopy /f /y "%qt_dir%\libstdc++-6.dll" "%projects_dir%\standalone"
xcopy /f /y "%qt_dir%\libgcc_s_seh-1.dll" "%projects_dir%\standalone"
xcopy /f /y "%qt_dir%\libwinpthread-1.dll" "%projects_dir%\standalone"
