#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QApplication>
#include <QMimeData>

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPlainTextEdit>
#include <QFile>
#include <QDir>
#include <QFileInfo>

#include <QDebug>

#define DEBUG 0

#if DEBUG
#define Q_FUNC_INFO_ENTER qDebug() << "enter:" << Q_FUNC_INFO;
#define Q_FUNC_INFO_EXIT  qDebug() << "exit:" << Q_FUNC_INFO;
#else
#define Q_FUNC_INFO_ENTER
#define Q_FUNC_INFO_EXIT
#endif

class MainWindow : public QMainWindow
{
Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

    void func(const QString &dir);

    QWidget *w;
    QBoxLayout *mainLayout;
    QPlainTextEdit *qPlainTextEdit;

private:
    void dropEvent(QDropEvent *event) override;
    void dragMoveEvent(QDragMoveEvent *event) override;
    void dragEnterEvent(QDragEnterEvent *event) override;
    void dragLeaveEvent(QDragLeaveEvent *event) override;
};
#endif // MAINWINDOW_